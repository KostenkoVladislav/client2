import React from "react";

import Footer from "./components/static/Footer";
import Header from "./components/static/Header";
import Router from "./Router";
import "./app.scss";


export default function App() {
    return (
        <div className="app-container container">
            <Header />
            <Router />
            <Footer />
        </div>
    )
}