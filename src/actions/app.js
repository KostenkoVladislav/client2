export const sendProductsAction = (data, dispatch) => 
{
    
    fetch('http://localhost:3000/add-orders', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    })
    .then((res)=>{
        return res.json();
    })
    .then((d)=>{
        if(d.success)
        {
            dispatch({type:'SHOW_MESSAGE_SUCCESS'});
            setTimeout(()=>{
                dispatch({type:'CLEAR_CART'});
                dispatch({type:"SHOW_CART"});
                dispatch({type:"SHOW_ALERT"});
            },2000)
        }
    });
}

export const getProductsAction = (dispatch) => {
    fetch('http://localhost:3000/get-products', {
        method: 'POST'
    })
    .then((res)=>{
        return res.json();
    })
    .then((d)=>{
        dispatch({type:'SET_PRODUCTS', data:d});
    });
}

export const removeProductAction = (dispatch, d)=>{
    dispatch({type:"REMOVE_PRODUCT", data:d});
}

export const showCartAction = (dispatch)=>{
    dispatch({type:"SHOW_CART"});
}

export const showAlertAction = (dispatch)=>{
    dispatch({type:"SHOW_ALERT"});
}

export const addCartAction = (dispatch, d)=>{
    dispatch({type:"ADD_PRODUCT", data:d});
}

export const changeCustomerAction = (d, dispatch)=>{
    dispatch({type:"CHANGE_CUSTOMER", data:d});
}

export const changeCustomerErrorsAction = (d, dispatch)=>{
    dispatch({type:"CHANGE_CUSTOMER_ERRORS", data:d});
}

export const setProductAction = (d, dispatch)=>{
    dispatch({type:"SET_PRODUCT", data:d});
}