import React, {Component} from "react";
import {connect} from "react-redux";
import {setProductAction} from "../../actions/app";

class Product extends Component{

    componentDidMount(){
        let slug = this.props.match.params.slug;
        let product = this.getProductById(slug);
        this.props.setProduct(product);
    }

    getProductById(id)
    {
        for(var j in this.props.products)
        {
            if(this.props.products[j]['_id'] === id)
            {
                return this.props.products[j];
            }
        }
        return false;
    }

    render(){
        return (
            <div className="page">
                <h4>{this.props.product.name}</h4>
                <div className="img-block">
                    <img src={'/'+this.props.product.image} className="img-fluid" />
                </div>
                <div>
                    {this.props.product.price}
                </div>
                <div>
                    {this.props.product.manufacturer}
                </div>
            </div>
        )
    }
}

export default connect( 
    state => ({
        products:state.app.products,
        product:state.product
    }),
    dispatch => ({
        setProduct:(d)=>{
            setProductAction(d, dispatch);
        }
    })
)(Product);