import React, {Component} from "react";
import Product from "../modules/Product";
import Alert from "../modules/Alert";
import {connect} from "react-redux";
import {getProductsAction} from "../../actions/app";

const Show = (props) => {
    return props.products.map((product, k)=> <Product addCart={props.addCart} key={k} pkey={k} data={product} />);
}

class Products extends Component {

    componentDidMount() {
        this.props.getProducts();
    }

    render(){
        return (         
            <div className="page">
                {
                    this.props.showAlert ?
                        <Alert />
                    :''
                }
                <div className="row">
                    <Show products = {this.props.products} />
                </div>
            </div>
        )
    }
}


export default connect( 
    state => ({
        products:state.app.products,
        showAlert:state.app.showAlert
    }),
    dispatch => ({
        getProducts:()=>{
            getProductsAction(dispatch);
        }
    })
)(Products);