import React from "react";

import CartProduct from "../CartProduct";
import {showAlertAction} from '../../../actions/app';
import './style.scss';
import {connect} from "react-redux";

const Show = (props)=> {
    return props.products.map((el, k) => <CartProduct data = {el} key={k} />);
}

const Cart = (props) =>  {

    return (
        <div className="cart">

            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <Show products = {props.products} />
                </tbody>
            </table>
            
            <div className="form-group">
                <button onClick={props.showAlert} className="btn btn-success ml-3">Create order</button>
            </div>
        </div>
    )
    
}


export default connect( 
    state => ({
       products:state.cart.products,
       message:state.cart.messageSuccess
    }),
    dispatch => ({
       showAlert:()=>{
            showAlertAction(dispatch);
       }
    })
)(Cart);