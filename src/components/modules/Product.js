import React from "react";
import {connect} from "react-redux";
import {addCartAction} from '../../actions/app';
import {Link} from 'react-router-dom';

const Product = (props) => {

    let add = () =>{
        props.addCart(props.data);
    }

    return (
        <div className="product col-md-4">
            <div className="prod-wrap">
                <h4>{props.data.name}</h4>
                <div className="img-block">
                    <img src={props.data.image} className="img-fluid" />
                </div>
                <div>
                    {props.data.price}
                </div>
                <div>
                    {props.data.manufacturer}
                </div>
                <button onClick={add} data-pkey={props.pkey} type="button" className="btn btn-primary">Add to cart</button>
                <Link className="btn btn-success ml-3" to={'/product/'+props.data._id} >More</Link>
            </div>
        </div>
    )
}

export default connect( 
    state => ({
       
    }),
    dispatch => ({
        addCart: (d)=>{
            addCartAction(dispatch, d);
        }
    })
)(Product);