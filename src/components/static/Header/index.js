import React from "react";
import { Link } from 'react-router-dom';
import {connect} from "react-redux";
import './style.scss';

import Cart from "../../modules/Cart";

const menu = require('../../../system/menu.json');

const Show = ()=>{
    return menu.map((el, k)=><li className="nav-item" key={k} ><Link className="nav-link" to={el.link} >{el.name}</Link></li>);
}

const Header = (props) => {

    return (
        <header className="d-flex justify-content-between">
            <nav className="nav">
                <Show />
            </nav>
            <div className="cart-container">
                <table>
                    <thead>
                        <tr>
                            <th>Total Price</th>
                            <th>{props.totalSum}</th>
                            <th>Total Quantity</th>
                            <th>{props.totalQuantity}</th>
                        </tr>
                    </thead>
                </table>
                <i onClick={props.showCart} className="fa fa-cart-arrow-down" aria-hidden="true"></i>
                {props.show_cart ?
                    <Cart productsCart = {props.productsCart} />
                :''}
            </div>
        </header>
    )
    
}



export default connect( 
    state => ({
        show_cart:state.app.showCart,
        totalSum:state.cart.totalSum,
        totalQuantity:state.cart.totalQuantity
    }),
    dispatch => ({
        showCart: ()=>{
            dispatch({type:"SHOW_CART"});
        }
    })
)(Header);